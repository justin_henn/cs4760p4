//slave
//This is the slave file for the oss/slave program
//Justin Henn
//Assignment 4
//3/19/17
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/msg.h>
#include <unistd.h>
#include "msgqueue.h"
#include "variables.h"
#define PERM (S_IRUSR | S_IWUSR)

//structure for message

typedef struct {
   long mtype;
   char mtext[1024];
} mymsg_t;


//struct for pcb

typedef struct pcb_info {

   long pid;
   long time_to_live;
   long time_used;
   long process_num;
   long quantum;
   long queue_number;
   long start_wait_time;
}pcb_info_t;

int main (int argc, char **argv) {

  key_t key_seconds, key_pcb_info, key_nano_seconds, child_queue_key, parent_queue_key, kill_queue_key;
  key_pcb_info = 3423563;
  key_nano_seconds = 12345;
  child_queue_key = 85733;
  parent_queue_key = 922435;
  key_seconds = 84323;
  kill_queue_key = 000001;
  int* seconds;
  int* nano_seconds;
  //int proc_num = atoi(argv[1]);
  //int num_incr = atoi(argv[3]);
  //int number_of_proc = atoi(argv[4]);
  int shm_id_pcb_info, i, shm_id_nano_seconds, j, shm_id_seconds, io_quantum = 0, should_io = 3;
  //FILE* logfile;
  //struct timespec tps;
  static int child_queueid, parent_queueid, kill_queueid;
  int size, proc_num = atoi(argv[1]);
  mymsg_t mymsg, sentmsg, parentmsg;
  srand(time(NULL));
  int full_nano = 0, timer = 0, x = 0;
  int what_proc_am_i, what_message_to_check;
  pcb_info_t* pcb_for_project;
  srand(time(NULL));


//get shared memory

  if ((shm_id_pcb_info = shmget(key_pcb_info, proc_num*sizeof(pcb_info_t), PERM)) == -1) {
    perror("FAIL");
    return 1;
  }
  if ((pcb_for_project = (pcb_info_t*)shmat(shm_id_pcb_info, NULL, 0)) == (void *)-1) {
    perror("Failed to attach shared memory segment");
    if (shmctl(shm_id_pcb_info, IPC_RMID, NULL) == -1)
      perror("Failed to  remove memory segment");
      return 1;
  }


  if ((shm_id_seconds = shmget(key_seconds, sizeof(int), PERM)) == -1) {
    perror("shmget");
    return 1;
  }
  if ((seconds = (int *)shmat(shm_id_seconds, NULL, 0)) == (void *)-1) {
    perror("Failed to attach shared memory segment");
    return 1;
  }


  if ((shm_id_nano_seconds = shmget(key_nano_seconds, sizeof(int), PERM)) == -1) {
    perror("shmget");
    return 1;
  }
  if ((nano_seconds = (int *)shmat(shm_id_nano_seconds, NULL, 0)) == (void *)-1) {
      perror("Failed to attach shared memory segment");
      return 1;
  }

//get message queues

  child_queueid = initqueue(child_queue_key, child_queueid);
  parent_queueid = initqueue(parent_queue_key, parent_queueid);
  kill_queueid = initqueue(kill_queue_key, kill_queueid);

//loop to get the pcb it is

 while(1) {
  if(getpid() == (&pcb_for_project[x])->pid) {


    what_proc_am_i = (&pcb_for_project[x])->process_num;
    what_message_to_check = (&pcb_for_project[x])->process_num + 2;
    parentmsg.mtype = what_message_to_check;


    break;

  }
  x++;
}

//loop for critical section

  while(1) {


//if the child received a message from the parent then go


    if ((msgrcv(child_queueid, &sentmsg, 1024, what_message_to_check /*getpid()*/, 0)) >=0)
       ;

/*while(1) {
  if(getpid() == (&pcb_for_project[x])->pid) {


    what_proc_am_i = (&pcb_for_project[x])->process_num;
    what_message_to_check = (&pcb_for_project[x])->process_num + 2;
    parentmsg.mtype = what_message_to_check;


    break;

  }
  x++;
}*/

    //printf("%d, %d, %d\n", what_message_to_check, getpid(), sentmsg.mtype);
    //printf("in pid %d\n", getpid());


    should_io = rand() % io_range;

//if it got IO

    if (should_io == io_check) {

        io_quantum = rand() % ((atoi(sentmsg.mtext)) + 1);
        (&pcb_for_project[x])->time_used = (&pcb_for_project[x])->time_used + io_quantum;
        (&pcb_for_project[x])->quantum = io_quantum;

    }

//if no IO then update regularly

    else {
        (&pcb_for_project[x])->time_used = (&pcb_for_project[x])->time_used + (atoi(sentmsg.mtext));
      //  printf("%d\n", pcb_for_project->time_to_live);
      //  printf("%d\n", pcb_for_project->time_used);
            (&pcb_for_project[x])->quantum = atoi(sentmsg.mtext);
    }

//if the process is finished

     if ((&pcb_for_project[x])->time_used >= (&pcb_for_project[x])->time_to_live) {

          sprintf(parentmsg.mtext,"Terminated");
          if(msgsnd(parent_queueid, &parentmsg, 1024,0) == -1) {
           perror("Failed write");
           return 1;
      }
	 

      if ((msgrcv(kill_queueid, &sentmsg, 1024, 500, 0)) >=0) {
        //printf("pid, %s\n", sentmsg.mtext);
        //printf("got message\n");

        //printf("%d %d, %d\n", (&pcb_for_project[x])->time_to_live, getpid(), what_proc_am_i);
        break;
      }
    }

//if the process got IO and wasn't finished

    else if (should_io == io_check) {
          sprintf(parentmsg.mtext,"IO");
          if(msgsnd(parent_queueid, &parentmsg, 1024,0) == -1) {
           perror("Failed write");
           return 1;

          }
    }

//if the process wasn't finished and didn't get IO

    else {

      sprintf(parentmsg.mtext, "NO IO");
          if(msgsnd(parent_queueid, &parentmsg, 1024,0) == -1) {
        perror("Failed write");
        return 1;
        }
     }
    }
  return 0;
}
