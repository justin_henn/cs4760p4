//msgqueue.h
//This is the h file for msgqeueu
//Justin Henn
//Assignemnt 3
//3/05/17


#ifndef MSGQUEUE_H
#define MSGQUEUE_H

int initqueue(int key, int queueid);
int initchildprocqueue(int key, int queueid);
int msgprintf(int queueid, char *fmt, ...);
int msgread(int queueid, void *buf, int len);
int msgwrite( void *buf, int lne);
int removequeue(int queueid);

#endif
