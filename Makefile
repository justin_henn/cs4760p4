CC	= gcc		# The C compiler
CFLAGS	= -g		# Enable debugging by default
TARGET = oss slave

all: variables.o msgqueue.o queue.o oss slave

oss:	main.c
	$(CC) $(CFLAGS) -o oss main.c msgqueue.o queue.o variables.o

slave: slave.c
	$(CC) $(CFLAGS) -o slave slave.c msgqueue.o variables.o

msgqueue.o: msgqueue.c
	$(CC) $(CFLAGS) -c msgqueue.c

queue.o: queue.c
	$(CC) $(CFLAGS) -c queue.c

variables.o: variables.c
	$(CC) $(CFLAGS) -c variables.c

clean:
	/bin/rm -f *.o *~ *.txt *.out $(TARGET)
