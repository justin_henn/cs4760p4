//main
//This is the main file and the master file for the oss/slave program
//Justin Henn
//3/19/17
//Assignment 4


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <sys/msg.h>
#include <memory.h>
#include "queue.h"
#include "msgqueue.h"
#include "variables.h"
#define PERM (S_IRUSR | S_IWUSR)


/*typedef struct queue{
  struct Node *head, *tail;
} Queue;

typedef struct pcb_info {

   pid_t pid;
   long time_to_live;
   long time_used;
   long process_num;
}pcb_info_t;*/

//message struct

typedef struct {
   long mtype;
   char mtext[1024];
} mymsg_t;


//variables for queue and shared memory

int num_proc_for_alarm = 0, shm_id_pcb_info, shm_id_nano_seconds, shm_id_seconds;
pcb_info_t* pcb_for_project;
static int child_queueid, parent_queueid, kill_queueid;
int* seconds;
int* nano_seconds;
Queue* first_queue;
Queue* second_queue;
Queue* third_queue;
FILE* logfile;


//function to kill child processes

void kill_childs() {

  int y, z = num_proc_for_alarm;
  signal(SIGTERM, SIG_IGN);
  kill(0, SIGTERM);
  for(y = 0; y < num_proc_for_alarm; y++){

    if (wait(NULL) != -1) {

      z--;
    }
  }
}

//detach and remove shared memory
int detachandremove(int shmid, void *shmaddr) {
   int error = 0;

   if (shmdt(shmaddr) == -1)
     error = errno;
   if ((shmctl(shmid, IPC_RMID, NULL) == -1) && !error)
     error = errno;
   if (!error)
      return 0;
   return -1;
}

//Alarm handler

void ALARM_handler(int sig) {

/*  int y;
  fprintf(stderr, "Reached alarm\n");
  for(y = 0; y < num_proc_for_alarm; y++)
    kill(point[y], SIGKILL); */


  printf("Alarm Reached");
  kill_childs();

  if (detachandremove(shm_id_seconds, seconds) == -1) {
    perror("Failed to destroy shared memory segment");
    exit (1);
  }
  if (detachandremove(shm_id_nano_seconds, nano_seconds) == -1) {
    perror("Failed to destroy shared memory segment");
    exit (1);
  }
  if (detachandremove(shm_id_pcb_info, pcb_for_project) == -1) {
    perror("Failed to destroy shared memory segment");
    exit (1);
  }

  if(removequeue(parent_queueid) < 0)
    perror("remove queue");
  if(removequeue(child_queueid) < 0)
    perror("remove queue");
  if(removequeue(kill_queueid) < 0)
    perror("remove queue");
   
  queueDestroy(first_queue);
  queueDestroy(second_queue);
  queueDestroy(third_queue);

  fclose(logfile);

  exit(0);
}

//Control C handler

void SIGINT_handler(int sig) {
  signal(sig, SIG_IGN);
  printf("Received Control C\n");
  kill_childs();
    if (detachandremove(shm_id_seconds, seconds) == -1) {
    perror("Failed to destroy shared memory segment");
    exit (1);
  }
  if (detachandremove(shm_id_nano_seconds, nano_seconds) == -1) {
    perror("Failed to destroy shared memory segment");
    exit(1);
  }
  if (detachandremove(shm_id_pcb_info, pcb_for_project) == -1) {
    perror("Failed to destroy shared memory segment");
    exit(1);
  }

  if(removequeue(parent_queueid) < 0) 
    perror("remove queue");

  if(removequeue(child_queueid) < 0) 
    perror("remove queue");

  if(removequeue(kill_queueid) < 0)
    perror("remove queue");
   
  queueDestroy(first_queue);
  queueDestroy(second_queue);
  queueDestroy(third_queue);

 fclose(logfile);

  exit(0);
}




int main (int argc, char **argv) {

  int opt;
  char* filename = NULL;
  int number_proc = 0, number_wait = 0, i, x, processes_made = 0, z, num_lines = 0, total_overhead = 0, idle_time = 0, total_wait_time = 0;
  extern char* optarg;
  key_t key_pcb_info, key_nano_seconds, child_queue_key, parent_queue_key, key_seconds, kill_queue_key;
  key_pcb_info = 3423563;
  key_seconds = 84323;
  key_nano_seconds = 12345;
  child_queue_key = 85733;
  parent_queue_key = 922435;
  kill_queue_key = 000001;
  mymsg_t mymsg, sentmsg;

   srand(time(NULL));
  pid_t child_pid;
  unsigned char bit_vector[3];
  int iter = 0, iter1 = 0, checked_message, overhead = 0, when_to_spawn = rand() % 50001, first_ttl = 0, dispatch = 0;
  char num_procs_passed[1024];

//  printf("%d\n", first_queue_quantum);

  memset(bit_vector, 0, sizeof(bit_vector));



//Get arguments

  while ((opt = getopt(argc, argv, "hs:l:t:")) != -1) { //this loop looks at the the arguments and processes them appropriately

    switch (opt) {
    case 'h':
      printf("h - help\nn - specify a value after n for the error code \nl - specify a file name after l for the logfile \ns - number of slave processes\nt - seconds when program should terminate\n");
      return 0;
    case 's':
      number_proc = atoi(optarg);
      break;
    case 'l':
      filename = optarg;
      break;
    case 't':
      number_wait  = atoi(optarg);
      break;
    }
  }

  if (filename == NULL) { //if no filename was givien as an argument

     filename = "test.out";
  }

  if (number_proc == 0) { //if no number of processes was given as argument

    number_proc = 4;
  }
  if (number_wait == 0) { //if no wait time was given as argument

    number_wait = 20;
  }

  if ((logfile = fopen(filename, "w+")) == NULL) { //check to see if it opened a file

      perror("File open error");
      return -1;
  }


    //get shared memory for pcb_info
  if ((shm_id_pcb_info = shmget(key_pcb_info, number_proc*sizeof(pcb_info_t), PERM | IPC_CREAT)) == -1) {
    perror("FAIL");
    return 1;
  }
  if ((pcb_for_project = (pcb_info_t *)shmat(shm_id_pcb_info, NULL, 0)) == (void *)-1) {
    perror("Failed to attach shared memory segment");
    if (shmctl(shm_id_pcb_info, IPC_RMID, NULL) == -1)
      perror("Failed to  remove memory segment");
      return 1;
  }

  if ((shm_id_seconds = shmget(key_seconds, sizeof(int), PERM | IPC_CREAT)) == -1) {
    perror("FAIL");
    return 1;
  }
  if ((seconds = (int *)shmat(shm_id_seconds, NULL, 0)) == (void *)-1) {
    perror("Failed to attach shared memory segment");
    if (shmctl(shm_id_seconds, IPC_RMID, NULL) == -1)
      perror("Failed to  remove memory segment");
      return 1;
  }

  //get shared memory for nano_seconds;
  if ((shm_id_nano_seconds = shmget(key_nano_seconds, sizeof(int), PERM | IPC_CREAT)) == -1) {
    perror("FAIL");
    return 1;
  }
  if ((nano_seconds = shmat(shm_id_nano_seconds, NULL, 0)) == (void *)-1) {
    perror("Failed to attach shared memory segment");
    if (shmctl(shm_id_nano_seconds, IPC_RMID, NULL) == -1)
      perror("Failed to  remove memory segment");
      return 1;
  }

//create pcb queues
  first_queue = make_queue();
  second_queue = make_queue();
  third_queue = make_queue();

  //create queues

  child_queueid = initqueue(child_queue_key, child_queueid);
  parent_queueid = initqueue(parent_queue_key, parent_queueid);
  kill_queueid = initqueue(kill_queue_key, kill_queueid);

//initialize seconds and nano seconds and create string to past to slave processes for how many processes are running
  *seconds = 0;
  *nano_seconds = 0;
  sprintf(num_procs_passed, "%d", number_proc);


//make sure the first process wont end before the second process begins becuase then timer wont go up
//if the first process ended early

 /* while(first_ttl < when_to_spawn) {

   first_ttl = (rand() % range_for_ttl + 1) + (*seconds * 100000000) + *nano_seconds;

  } */

//fork first child process

  child_pid = fork();

  if (child_pid < 0) {

    printf("Could not fork!");
    return 1;
  }

//configure process pcb
  else if (child_pid > 0)  {

//          printf("%d, %d\n", iter, iter1);
    pcb_for_project->pid = child_pid;
    pcb_for_project->process_num = 0;
    pcb_for_project-> time_to_live = first_ttl; /* (rand() % 500000 + 1) + (*seconds * 100000000) + *nano_seconds;*/
//    printf("%d\n", pcb_for_project-> time_to_live);
//    printf("%d\n", when_to_spawn);
    pcb_for_project->time_used = 0;
        pcb_for_project->quantum = 0;
    pcb_for_project->queue_number = 1;
    pcb_for_project->start_wait_time = (*seconds * 100000000) + *nano_seconds; 
    bit_vector[iter] |= (1 << iter1);
    enqueue(first_queue, pcb_for_project);
    //overhead = rand() % 1001;
    //*nano_seconds = *nano_seconds + overhead;
    fprintf(logfile,"OSS: Generating process with PID %d and putting it in queue 1 at time %d:%d\n", child_pid, *seconds, *nano_seconds);
    num_lines++;
    num_proc_for_alarm++;
    //l
    //
    //(&pcb_for_project[1])->time_used = 45;

  }


//run chiles process
  else  {

    execl("./slave", "slave", num_procs_passed, 0);
  }

  //int this_sucks = 2;
  //sprintf(mymsg.mtext, "100000");

//parent process


  signal(SIGINT, SIGINT_handler);
  signal(SIGALRM, ALARM_handler);
  alarm(number_wait);

//loop until it's been 2 seconds

  while(*seconds < 2) {

    //printf("Sent\n");

//check queues for processes
    if(!queueEmpty(first_queue)) {

      pcb_info_t* pcb_temp;
      pcb_temp = dequeue(first_queue);
      mymsg.mtype = pcb_temp->process_num + 2; // pcb_temp->pid;
      sprintf(mymsg.mtext, "%d", first_queue_quantum);
      //printf("%d\n",mymsg.mtype);
      if(msgsnd(child_queueid, &mymsg, 1024,0) == -1) {
        perror("Failed write");
        return 1;
      }
        overhead = rand() % overhead_range;
    *nano_seconds = *nano_seconds + overhead;
     total_overhead = total_overhead + overhead;
     total_wait_time = total_wait_time + (((*seconds * 100000000) + *nano_seconds) - pcb_temp->start_wait_time);
    fprintf(logfile,"OSS: Dispatching PID %d from queue 1 at time  %d:%d\n", pcb_temp->pid, *seconds, *nano_seconds);
    fprintf(logfile, "OSS: total time this dispatch was %d\n", overhead);
    num_lines = num_lines + 2;
    dispatch =1;

        }
    else if(!queueEmpty(second_queue)) {

      pcb_info_t* pcb_temp;
      pcb_temp = dequeue(second_queue);
      mymsg.mtype = pcb_temp->process_num + 2; // pcb_temp->pid;
      sprintf(mymsg.mtext, "%d", second_queue_quantum);
      //printf("%d\n",mymsg.mtype);
          if(msgsnd(child_queueid, &mymsg, 1024,0) == -1) {
        perror("Failed write");
        return 1;
      }
        overhead = rand() % overhead_range;
    *nano_seconds = *nano_seconds + overhead;
     total_overhead = total_overhead + overhead; 
     total_wait_time = total_wait_time + (((*seconds * 100000000) + *nano_seconds) - pcb_temp->start_wait_time);
    fprintf(logfile,"OSS: Dispatching PID %d from queue 2 at time  %d:%d\n", pcb_temp->pid, *seconds, *nano_seconds);
    fprintf(logfile, "OSS: total time this dispatch was %d\n", overhead);
    num_lines = num_lines + 2;
    dispatch = 1;

      }

    else if(!queueEmpty(third_queue)) {

      pcb_info_t* pcb_temp;
      pcb_temp = dequeue(third_queue);
      mymsg.mtype = pcb_temp->process_num + 2; // pcb_temp->pid;
      sprintf(mymsg.mtext, "%d", third_queue_quantum);
      //printf("%d\n",mymsg.mtype);
          if(msgsnd(child_queueid, &mymsg, 1024,0) == -1) {
        perror("Failed write");
        return 1;
      }
              overhead = rand() % overhead_range;
    *nano_seconds = *nano_seconds + overhead;
     total_overhead = total_overhead + overhead; 
     total_wait_time = total_wait_time + (((*seconds * 100000000) + *nano_seconds) - pcb_temp->start_wait_time);
    fprintf(logfile,"OSS: Dispatching PID %d from queue 3 at time  %d:%d\n", pcb_temp->pid, *seconds, *nano_seconds);
    fprintf(logfile, "OSS: total time this dispatch was %d\n", overhead);
    num_lines = num_lines + 2;
    dispatch = 1;

        }


//if there was no process that ran
    else{

     idle_time++;
     *nano_seconds = *nano_seconds + 1;
    //  printf("%d\n", ((*seconds *100000000) + *nano_seconds) >= when_to_spawn);
      dispatch = 0;
   }
  

//check to see if should create a new process

    if(((*seconds * 100000000) + *nano_seconds) >= when_to_spawn ) {




      when_to_spawn = (rand() % 50001) + (*seconds * 100000000) + *nano_seconds;
      for (z = 0; z < number_proc; z++) {

        iter = z / 8;
        iter1 = z % 8;


          //printf("%d, %d ", iter, iter1);

    //printf("this is vector %d\n", bit_vector[iter] & (1 << iter1));
          //printf("%d, %d\n", iter, iter1);
        if((bit_vector[iter] & (1 << iter1)) == 0) {


          child_pid = fork();

        if (child_pid < 0) {

           printf("Could not fork!");
           return 1;
        }
        else if (child_pid > 0)  {



    // printf("forked\n");
          (&pcb_for_project[z])->pid = child_pid;
          (&pcb_for_project[z])->process_num = z;
          (&pcb_for_project[z])->time_to_live = (rand() % range_for_ttl + 1) + (*seconds * 100000000) + *nano_seconds;
          (&pcb_for_project[z])->time_used = 0;
          (&pcb_for_project[z])->quantum = 0;
          (&pcb_for_project[z])->queue_number = 1;
          bit_vector[iter] |= (1 << iter1);
          enqueue(first_queue, (&pcb_for_project[z]));
          //(&pcb_for_project[1])->time_used = 45;
          num_proc_for_alarm++;

      //  overhead = rand() % 1001;
    //*nano_seconds = *nano_seconds + overhead;
    //fprintf(logfile,"Dispatched pid took %d\n", overhead);

          fprintf(logfile,"OSS: Generating process with PID %d and putting it in queue 1 at time %d:%d\n", child_pid, *seconds, *nano_seconds);
          num_lines++;
        }

        else  {

          execl("./slave", "slave", num_procs_passed, 0);
        }
        // printf("hi");
        //
            break;
       }

    }
   }

//check for messages from child
//needs to make sure there was an actual process that ran

if (dispatch == 1) {
    if ((msgrcv(parent_queueid, &sentmsg, 1024, 0, 0)) >=0) {
        // perror("Failed to read message queue");
        // return 1;
        //printf("%s\n", sentmsg.mtext);
                checked_message = sentmsg.mtype - 2;
        fprintf(logfile, "OSS: Receiving that process with PID %d ran for %d nanoseconds\n", (&pcb_for_project[checked_message])->pid, (&pcb_for_project[checked_message])->quantum);
        num_lines++;


//check to see if a process is finished

        if(!strcmp(sentmsg.mtext, "Terminated")) {


          *nano_seconds = *nano_seconds + (&pcb_for_project[checked_message])->quantum;
          num_proc_for_alarm--;
          iter = checked_message / 8;
          iter1 = checked_message % 8;
          (bit_vector[iter] &= ~(1 <<iter1));

         // printf("%s\n", sentmsg.mtext);
          mymsg.mtype = 500;
          sprintf(mymsg.mtext, "You can die");
        //printf("%s\n", mymsg.mtext);
          if(msgsnd(kill_queueid, &mymsg, 1024,0) == -1) {
            perror("Failed write");
            return 1;
          }
        //break;
        //*seconds = *seconds + 1;

  //       printf("Made it\n");
      }
//if the process was not finished

      else {

//check if it got IO

           if(!strcmp(sentmsg.mtext, "IO")) {
              enqueue(first_queue, (&pcb_for_project[checked_message]));
              *nano_seconds = *nano_seconds + (&pcb_for_project[checked_message])->quantum;
              (&pcb_for_project[checked_message])->queue_number = 1;
              fprintf(logfile, "OSS: not using its entire quantum\n");
              fprintf(logfile, "OSS: Putting process with with PID %d into queue 1\n", (&pcb_for_project[checked_message])->pid);
              num_lines = num_lines + 2;
              (&pcb_for_project[checked_message])->start_wait_time = (*seconds * 100000000) + *nano_seconds;

           }

//If it was in the first queue put it in the second queue

           else if((&pcb_for_project[checked_message])->queue_number == 1) {

               enqueue(second_queue, (&pcb_for_project[checked_message]));
              *nano_seconds = *nano_seconds + (&pcb_for_project[checked_message])->quantum;
              (&pcb_for_project[checked_message])->queue_number = 2;
              fprintf(logfile, "OSS: Putting process with with PID %d into queue 2\n", (&pcb_for_project[checked_message])->pid);
              num_lines++;
              (&pcb_for_project[checked_message])->start_wait_time = (*seconds * 100000000) + *nano_seconds;

           }

//if it didn't get IO and wasn't in the first queue

           else {

               enqueue(third_queue, (&pcb_for_project[checked_message]));
              *nano_seconds = *nano_seconds + (&pcb_for_project[checked_message])->quantum;
              fprintf(logfile, "OSS: Putting process with with PID %d into queue 3\n", (&pcb_for_project[checked_message])->pid);
              num_lines++;
              (&pcb_for_project[checked_message])->start_wait_time = (*seconds * 100000000) + *nano_seconds;

           }


         }
        }
    }
    waitpid(-1, 0, WNOHANG);

//if nano seoncds got high enough to make a second

    if (*nano_seconds >= 100000000) {

        *nano_seconds = *nano_seconds - 100000000;
        *seconds = *seconds + 1;
    }

//if 2 seconds reached

    if(*seconds >= 2)
      printf("2 seconds reached\n");

//if number of lines to text file are over 10000

    if(num_lines >= 10000) {
       printf("Reached 10000 lines\n");
       break;
    }

  }

//kill child processes

  kill_childs();
  fprintf(logfile,"\nSTATS\n----------\ntotal overhead: %d\naverage overhead: %.6f\n", total_overhead, (float)total_overhead/((*seconds * 100000000) + *nano_seconds));
  fprintf(logfile,"total idle time: %d\naverage idle time: %.6f\n", idle_time, (float)idle_time/((*seconds * 100000000) + *nano_seconds));
  fprintf(logfile,"total wait time: %d\naverage wait time: %.6f\n", total_wait_time, (float)total_wait_time/((*seconds * 100000000) + *nano_seconds));
//close logfile

  fclose(logfile);

//detach memory

  if (detachandremove(shm_id_pcb_info, pcb_for_project) == -1) {
    perror("Failed to destroy shared memory segment");
    return 1;
  }

  if (detachandremove(shm_id_seconds, seconds) == -1) {
    perror("Failed to destroy shared memory segment");
    return 1;
  }
  if (detachandremove(shm_id_nano_seconds, nano_seconds) == -1) {
    perror("Failed to destroy shared memory segment");
    return 1;
  }

//free queues

  queueDestroy(first_queue);
  queueDestroy(second_queue);
  queueDestroy(third_queue);


//remove messages queues

  if(removequeue(parent_queueid) < 0)
    perror("remove queue");
  if(removequeue(child_queueid) < 0)
    perror("remove queue");
  if(removequeue(kill_queueid) < 0)
    perror("remove queue");



  return 0;


}

