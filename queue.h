//queue.h
//This is the h file for queues
//Author: Justin Henn
//Date 3/19/2017
//Assignment: 4
#ifndef QUEUE_H
#define QUEUE_H

#include <sys/types.h>

typedef struct queue{
  struct Node *head, *tail;
} Queue;

typedef struct node{
    int data;
    struct node* next;
} Node;

typedef struct pcb_info {

   pid_t pid;
   long time_to_live;
   long time_used;
   long process_num;
   long quantum;
   long queue_number;
   long start_wait_time;
}pcb_info_t;

Queue* make_queue();
pcb_info_t* dequeue(Queue *this_queue);
void enqueue(Queue *this_queue, pcb_info_t* x);
void queueDestroy(Queue *q);
int queueEmpty(const struct queue *q);


#endif
