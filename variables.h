//variables.h: H file for queue program
//This is the h files for parameters
//Author: Justin Henn
//Date 3/19/2017
//Assignment: 4
#ifndef VARIABLE_H
#define VARIABLE_H


extern int first_queue_quantum;
extern int second_queue_quantum;
extern int third_queue_quantum;
extern int overhead_range;
extern int io_range;
extern int io_check;
extern int range_for_ttl;

#endif

