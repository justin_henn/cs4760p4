//queue.c
//This is the c file for the queues
//Justin Henn
//Date: 3/19/17
//Assignment 4


#include <sys/types.h>
#include <stdlib.h>

//struct for pcbs
//
typedef struct pcb_info {

   pid_t pid;
   long time_to_live;
   long time_used;
   long process_num;
   long quantum;
   long queue_number;
   long start_wait_time;
}pcb_info_t;


//struct for nodes

typedef struct node{
    pcb_info_t* data;
    struct node* next;
} Node;

//struct for queues

typedef struct queue{
   Node *head, *tail;
} Queue;

//make a queue


Queue* make_queue() {

    Queue *new_queue = (Queue*)malloc(sizeof(Queue));
    new_queue->head = NULL;
    new_queue->tail = NULL;
    return new_queue;
}

//enqueue node onto queue

void enqueue(Queue *this_queue, pcb_info_t* temp_pcb) {
    Node *temp;
    temp = malloc(sizeof(Node));
    temp->data = temp_pcb;
    temp->next = NULL;
    if(this_queue->head == NULL && this_queue->tail == NULL) { //empty
        this_queue->head = this_queue->tail = temp;
        return;
    }
    this_queue->tail->next = temp;
    this_queue->tail = temp;
}

//dequeue node off the queue

pcb_info_t* dequeue(Queue *this_queue) {
    Node* temp = this_queue->head;
    pcb_info_t* temp_pcb;
    if(this_queue->head == NULL) return;   //empty
    temp_pcb = this_queue->head->data;
    if(this_queue->head == this_queue->tail) {
        this_queue->head = this_queue->tail = NULL;
    }
    else {
        this_queue->head = this_queue->head->next;
    }
    free(temp);
    return temp_pcb;
}

//check if queue is empty

int queueEmpty(const struct queue *q) {

        return (q->head == 0);
}

//destroy the queue

void queueDestroy(struct queue *q) {

   while(!queueEmpty(q)) {
        dequeue(q);
    }

    free(q);
}

